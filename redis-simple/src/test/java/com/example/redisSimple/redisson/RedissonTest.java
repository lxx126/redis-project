package com.example.redisSimple.redisson;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;

public class RedissonTest {

    public static void main(String[] args) {
        Config config = new Config();
        SingleServerConfig serverConfig = config.useSingleServer();
        serverConfig.setAddress("redis://127.0.0.1:6379");

        RedissonClient client = Redisson.create(config);
        RLock rLock = client.getLock("redisson_lock");
        try {
            rLock.lock();
        } finally {
            rLock.unlock();
        }


    }
}
