package com.example.redisSimple.server;

import com.baomidou.mybatisplus.service.IService;
import com.example.redisSimple.entity.Commodity;
import org.springframework.stereotype.Service;

/**
 * Commodity
 *
 * @ClassName Commodity
 * @Author lixingxing
 * @Date 2021/08/09 下午 14:02
 * @Version 1.0
 */
public interface CommodityService extends IService<Commodity> {
}
