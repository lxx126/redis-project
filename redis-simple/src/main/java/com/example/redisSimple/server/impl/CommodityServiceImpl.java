package com.example.redisSimple.server.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.example.redisSimple.entity.Commodity;
import com.example.redisSimple.mapper.CommodityMapper;
import com.example.redisSimple.server.CommodityService;
import org.springframework.stereotype.Service;

/**
 * CommodityServiceImpl
 *
 * @ClassName CommodityServiceImpl
 * @Author lixingxing
 * @Date 2021/08/09 下午 14:03
 * @Version 1.0
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements CommodityService {

}
