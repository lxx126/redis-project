//package com.example.redisSimple.server;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.params.SetParams;
//
//import java.util.Arrays;
//
///**
// * RedisLock
// *
// * @ClassName RedisLock
// * @Author lixingxing
// * @Date 2021/08/03 上午 10:56
// * @Version 1.0
// */
//@Component
//public class RedisLock {
//
//    private static final String redis_lock_key = "redis_lock";
//
//    // 锁超时时间
//    private static final Long time_out = 99999L;
//    // 锁过期时间
//    private static final Long expired_time = 30000L;
//
//    SetParams params = SetParams.setParams().nx().px(expired_time);
//
//    @Autowired
//    private JedisPool jedisPool;
//
//    /**
//     * 加锁
//     * 1. 尝试加锁，查看返回是否正确。
//     * 2. 查看锁是否超时
//     * 3. 关闭jedispool
//     *
//     * @param id
//     * @return
//     */
//    public boolean lock (String id) {
//        Jedis jedis = jedisPool.getResource();
//        long start = System.currentTimeMillis();
//        try {
//            while (true) {
//                String set = jedis.set(redis_lock_key, id, params);
//                if ("OK".equals(set)) {
//                    System.out.println(Thread.currentThread().getName());
//                    return true;
//                }
//                long interval = System.currentTimeMillis() - start;
//                if ( interval > time_out) {
//                    return false;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            jedisPool.close();
//        }
//        return false;
//    }
//
//    /**
//     * 解锁
//     * @param id
//     * @return
//     */
//    public boolean unlock (String id) {
//        Jedis jedis = jedisPool.getResource();
//        String script =
//                "if redis.call('get',KEYS[1]) == ARGV[1] then" +
//                    "   return redis.call('del',KEYS[1]) " +
//                    "else" +
//                    "   return 0 " +
//                    "end";
//        try {
//            Object result = jedis.eval(script, Arrays.asList(redis_lock_key), Arrays.asList(id));
//            if ("1".equals(result.toString())) {
//                return true;
//            }
//            return false;
//        } finally {
//            jedisPool.close();
//        }
//    }
//
//
//}
