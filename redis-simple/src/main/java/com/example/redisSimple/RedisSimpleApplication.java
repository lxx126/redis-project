package com.example.redisSimple;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.redisSimple.mapper")
public class RedisSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisSimpleApplication.class, args);
    }

}
