//package com.example.redisSimple.controller;
//
//import com.example.redisSimple.server.RedisLock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.IdGenerator;
//import org.springframework.util.JdkIdGenerator;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
///**
// * LockController
// *
// * @ClassName LockController
// * @Author lixingxing
// * @Date 2021/08/03 上午 11:17
// * @Version 1.0
// */
//@RestController
//public class LockController {
//
//    @Autowired
//    private RedisLock redisLock;
//
//    @RequestMapping("/index")
//    public void index() {
//
//        int clientcount = 10;
//        try {
//            // 计数器
//            CountDownLatch countDownLatch = new CountDownLatch(clientcount);
//            ExecutorService executorService = Executors.newFixedThreadPool(clientcount);
//            long start = System.currentTimeMillis();
//            for (int i = 0 ;i < clientcount; i ++) {
//                executorService.execute( () -> {
//                    IdGenerator idGenerator = new JdkIdGenerator();
//                    String id = idGenerator.generateId().toString();
//                    System.out.println("获取锁");
//                    boolean lock = redisLock.lock(id);
//                    if (lock) {
//                        try {
//                            // 相关于执行业务
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    System.out.println("释放锁");
//                    redisLock.unlock(id);
//                });
//            }
//            long end = System.currentTimeMillis();
//            System.out.println("消耗时间： " + (start - end) + "。执行线程数：" + clientcount);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
