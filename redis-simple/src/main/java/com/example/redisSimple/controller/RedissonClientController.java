package com.example.redisSimple.controller;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class RedissonClientController {

    @Autowired
    private RedissonClient redissonClient;

    @RequestMapping("/redisson/index")
    public void getRedissonIndex () {
        RLock lock = null;
        try {
            lock = redissonClient.getLock("lock1");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName());
            System.out.println(new Date());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

}
