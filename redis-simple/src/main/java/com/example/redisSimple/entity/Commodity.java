package com.example.redisSimple.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Commodity
 *
 * @ClassName Commodity
 * @Author lixingxing
 * @Date 2021/08/09 下午 13:54
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Commodity extends Model<Commodity> {

    private Integer id ;

    private String commodityName;

    private Integer commodityNum;

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
