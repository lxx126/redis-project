package com.example.redisSimple.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.redisSimple.entity.Commodity;

/**
 * CommodityMapper
 *
 * @ClassName CommodityMapper
 * @Author lixingxing
 * @Date 2021/08/09 下午 14:04
 * @Version 1.0
 */
public interface CommodityMapper extends BaseMapper<Commodity> {
}
