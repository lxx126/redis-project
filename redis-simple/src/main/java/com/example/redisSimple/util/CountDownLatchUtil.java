package com.example.redisSimple.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CountDownLatchUtil
 *
 * CountDownLatch : 计数器，创建计数器需要指定线程数，通过countDown()减少计数器数量，计数器到0时，会释放await()。
 *
 * 使用到的两个方法
 * countDown :  递减锁存器的计数，如果计数达到零，则释放所有等待的线程。
 * await：如果记数不为零，则导致线程等待。
 *
 * @ClassName CountDownLatchUtil
 * @Author lixingxing
 * @Date 2021/08/03 下午 15:00
 * @Version 1.0
 */
public class CountDownLatchUtil {

//    public static void main(String[] args) {
//        final CountDownLatch latch = new CountDownLatch(2);
//        ExecutorService executorService = Executors.newFixedThreadPool(2);
//        executorService.execute(() -> {
//            try {
//                Thread.sleep(3000);
//                System.out.println("1子线程： + " + Thread.currentThread().getName() + "执行");
//                System.out.println("1子线程： + " + new Date());
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            latch.countDown();
//        });
//
//        executorService.execute(() -> {
//            try {
//                Thread.sleep(5000);
//                System.out.println("2子线程： + " + Thread.currentThread().getName() + "执行");
//                System.out.println("1子线程： + " + new Date());
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            latch.countDown();
//        });
//
//        System.out.println("等待两个线程执行完毕");
//        try {
//            latch.await();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("执行完毕");
//    }


    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, -10);
        Date y = calendar.getTime();
        String year = format.format(y);
        System.out.println("过去一年 ："+year);
    }
}
