package com.example.redisSimple.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;

@Configuration
public class JedisPoolConfig {

    @Bean
    public JedisPool getJedisPool () {
        return new JedisPool();
    }

}
