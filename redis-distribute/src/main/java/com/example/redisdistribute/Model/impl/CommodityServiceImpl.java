package com.example.redisdistribute.Model.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.example.redisdistribute.Model.Commodity;
import com.example.redisdistribute.Model.CommodityService;
import com.example.redisdistribute.mapper.CommodityMapper;
import org.springframework.stereotype.Service;

/**
 * CommodityServiceImpl
 *
 * @ClassName CommodityServiceImpl
 * @Author lixingxing
 * @Date 2021/08/09 下午 14:03
 * @Version 1.0
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements CommodityService {

}
