package com.example.redisdistribute.Model;

import com.baomidou.mybatisplus.service.IService;

/**
 * Commodity
 *
 * @ClassName Commodity
 * @Author lixingxing
 * @Date 2021/08/09 下午 14:02
 * @Version 1.0
 */
public interface CommodityService extends IService<Commodity> {
}
