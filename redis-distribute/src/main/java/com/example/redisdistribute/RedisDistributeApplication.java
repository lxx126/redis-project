package com.example.redisdistribute;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.example.redisdistribute.mapper")
@SpringBootApplication
public class RedisDistributeApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisDistributeApplication.class, args);
    }

}
