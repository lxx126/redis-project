package com.example.redisdistribute.common;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * MyRedisTemplate
 *
 * @ClassName MyRedisTemplate
 * @Author lixingxing
 * @Date 2021/08/11 下午 14:56
 * @Version 1.0
 */
@Component
public class MyRedisTemplate {

    @Bean
    public RedisTemplate<String,Object> getRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String,Object> redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(factory);

        return redisTemplate;
    }

}
