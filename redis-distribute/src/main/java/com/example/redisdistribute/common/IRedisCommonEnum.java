package com.example.redisdistribute.common;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * IRedisCommonEnum
 *
 * @ClassName IRedisCommonEnum
 * @Author lixingxing
 * @Date 2021/08/11 下午 14:42
 * @Version 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
public enum IRedisCommonEnum {
    KEY("KEY前缀","Lock:");


    private String name;
    private String value;

}
