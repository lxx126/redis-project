package com.example.redisdistribute.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * 分布式锁注解
 * @date 2021年8月11日
 * @author lixingxing
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface DistributedLock {

    /**
     * 默认
     * @return
     */
    String key() default "";

    /**
     * 过期时间，默认30秒，单位ms
     * 注意：过期时间一定要长于业务的执行时间
     * @return
     */
    long expire() default 30000L;

    /**
     * 获取锁超时时间，默认3秒 单位ms
     * @return
     */
    long timeOut() default 3000L;

    /**
     * 重试次数
     * @return
     */
    int retryTimes() default Integer.MAX_VALUE;

}
