package com.example.redisdistribute.controller;

import com.example.redisdistribute.common.DistributedLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * DistributeTest
 *
 * @ClassName DistributeTest
 * @Author lixingxing
 * @Date 2021/08/11 下午 16:15
 * @Version 1.0
 */
@Slf4j
@RestController
public class DistributeTest {


    @DistributedLock(retryTimes = 1000, timeOut = 1000)
    @RequestMapping("/test")
    public void test() {
        log.info("执行业务");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
