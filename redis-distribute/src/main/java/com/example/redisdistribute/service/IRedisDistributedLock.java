package com.example.redisdistribute.service;

/**
 * IRedisDistributedLock
 *
 * @ClassName IRedisDistributedLock
 * @Author lixingxing
 * @Date 2021/08/11 下午 14:36
 * @Version 1.0
 */
public interface IRedisDistributedLock {

    /**
     * 加锁
     * @param key
     * @param requireTimeOut 获取锁超时时间 单位ms
     * @param lockTimeOut 锁过期时间，一定要大于业务执行时间 单位ms
     * @param retries 尝试获取锁的最大次数
     * @return
     */
    boolean lock(String key, Long requireTimeOut, Long lockTimeOut, Integer retries);

    /**
     * 解锁
     * @param key
     * @return
     */
    boolean unLock(String key);

}
